app.factory('candidateApiServices',[ '$http', 'urlService', '$routeParams','Upload', function( $http, urlService, $routeParams,Upload){
    var service = {};

    service.candidate_create_update = function(candidateData, callback) {
        var candidateRequest = {};
        candidateRequest.password = "123456";
        candidateRequest.title = candidateData.personalDetail.title;
        candidateRequest.firstName = candidateData.personalDetail.firstName;
        candidateRequest.middleName = candidateData.personalDetail.middleName;
        candidateRequest.lastName = candidateData.personalDetail.lastName;
        candidateRequest.email = candidateData.personalDetail.personalEmailId;
        candidateRequest.username = candidateData.personalDetail.personalEmailId;
        candidateRequest.contactNo = candidateData.personalDetail.contactNo;
        candidateRequest.gender = candidateData.personalDetail.gender;
        candidateRequest.maritalStatus = candidateData.personalDetail.maritalStatus;
        $http({
            method : "POST",
            url : urlService.apiurl+'Candidates',
            data : candidateRequest
        }).then(function mySuccess(response) {
            var alldata = response.data;
             callback (null, {
                "candidateId" : alldata.id
             });
        }, function myError(response) {
            callback(response, null);
        });
    }

    service.candidatedetails_create_update = function(candidateId, candidateData, callback) {
        var candidateRequest = {};
        candidateData.candidateId = candidateId;

        $http({
            method : "POST",
            url : urlService.apiurl+'CandidateDetails',
            data : candidateData
        }).then(
            function mySuccess(response) {
                var alldata = response.data;
                 callback (null, {
                    "candidateDetailId" : alldata.id
                 });
            }, function myError(response) {
                callback(response, null);
            }
        );
    }

    /*
    * Upload resume file
    * @fileData : It is a file object that was selected
    * Return file response that has been uploaded with file path details
    * */
    service.resumeUploadAPIService = function (fileData, callback) {
        /*create new container*/
        $http({
            method : "POST",
            url : urlService.apiurl+'resumeBank/createContainerNew',
            data : {"data":{"name":fileData.uploadPath}}
        }).then(function mySuccess(response) {
            service.saveResumeUploadAPIService(fileData, function(error, data){
                callback(error, data);
            });
        }, function myError(response) {
            service.saveResumeUploadAPIService(fileData, function(error, data){
                callback(error, data);
            });
        });
    };

    service.saveResumeUploadAPIService = function (fileData, callback) {
        Upload.upload({
            method: 'POST',
            url: urlService.apiurl+'resumeBank/'+fileData.uploadPath+'/upload',
            data: {file:fileData.fileObject}
        }).then(function(resp) {
            callback(null, resp.data.result.files.file);
        }, function(resp) {
            callback(resp.status, null);
        }, function(evt) {
            //console.log(evt);
            /*callback(null, {
             "filename" : evt.config.data.file.name
             });*/
            /*var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
             console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);*/
        });
    };

    /*
     * Parse resume file
     * @fileData : It is a file object that was uploaded in resumeUploadAPIService service
     * Return file response that has been uploaded with file path details
     * */
    service.resumeParseAPIService = function (fileData, callback) {

    };

    service.getCandidateDetail = function(candidateUserName, callback) {
        if(candidateUserName != "") {
            var takeInput = {email:candidateUserName}

            $http({
                method : "GET",
                url : urlService.apiurl+'Candidates/getCandidateFullDetail?takeAction='+ JSON.stringify(takeInput)
            }).then(function mySuccess(response){
                callback(null, response);
            }, function myError(response){
                callback(error, null);
            });
        } else {
            callback("error", null);
        }
    }

    return service;
}]);
/**
 * Created by pramod on 12-Jun-18.
 */
var app = angular.module('app');
