var app = angular.module('app');
app.factory('rfrApiServices', function( $http, urlService, $routeParams){
	var service = {};
	

	/*
		Author : Manoj Gupta
		Description : this function is used to get requisition data
		Updated on : 11th Jun 2108
	*/
	service.requisitions_getData = function(requisitionId, callback) {
		$http({
			method : "POST",
			url : urlService.apiurl+'Requisitions/getData',
			data : {"requisitionId" : requisitionId},
		}).then(function mySuccess(response) {
			//console.log(response.data.data);
			var alldata = response.data.data;
			callback (null, {
						"requisitionId" : alldata.id,
						"title": alldata.title,
						"positionType": alldata.positionType,
						"other": alldata.other,
						"status": alldata.status
					});
		}, function myError(response) {
			callback(response, null);
		});
	}

	/*
		Author : Manoj Gupta
		Description : this function is used to save requisition data
		Updated on : 11th Jun 2108
	*/
	service.requisitions_saveData = function(data, callback) {
		$http({
            method : "POST",
            url : urlService.apiurl+'Requisitions/saveData',
            data : {'data':data},
        }).then(function mySuccess(response) {
            callback (null,response.data.data);
        }, function myError(response) {
            console.log(response);
        });
	}

	/*
		Author : Manoj Gupta
		Description : this function is used to update rfr approval status
		Updated on : 13th Jun 2108
	*/
	service.RequisitionDetails_updateApprovalStatus = function(data, callback) {
		$http({
            method : "POST",
            url : urlService.apiurl+'RequisitionDetails/updateApprovalStatus',
            data : {'data':data},
        }).then(function mySuccess(response) {
        	if(response.data.data.finalStatus == 3)
        	{
        		var updatedata = {'requisitionId':data.requisitionId,"status" : 3,"levels" : data.leveldata,"hosturl" : urlService.hosturl};
        		service.requisitions_updateStatus(updatedata,function(err,cb){
        			callback (null,response.data.data);
        		});
        		///call another service
        	}
        	else
        	{
        		callback (null,response.data.data);
        	}
        }, function myError(response) {
            console.log(response);
        });
	}

	/*
		Author : Manoj Gupta
		Description : this function is used to update rfr status
		Updated on : 14th Jun 2108
	*/
	service.requisitions_updateStatus = function(data, callback) {
		$http({
            method : "POST",
            url : urlService.apiurl+'Requisitions/updateStatus',
            data : {"data" : data},
        }).then(function mySuccess(response) {
            callback (null,response.data.data);
        }, function myError(response) {
            console.log(response);
        });
	}
	
	return service;
});
