
var app = angular.module('app', ["ngRoute","validation",'validation.rule','ngFileUpload']);


app.run(function($rootScope) {
    $rootScope.authToken = 'asdfiue2irh2iur93o';
    $rootScope.approvalLevels = 2;
    $rootScope.recruitmentStages = 5;
    $rootScope.companyName = 'sequelone';
    $rootScope.uploadResumeBy = 'vendor';
    $rootScope.vendorCode = 'v1';
    $rootScope.jobID = 'JOB0001';
    $rootScope.folderSeparator = '_';
});

app.config(function($routeProvider) {
        $routeProvider

            .when("/createVendors/:vendorId", {
                templateUrl : "views/vendor/createvendor.html",
                controller : "vendorController"
            })
        .when("/vendors", {
            templateUrl : "views/vendor/viewAllVendors.html",
            controller : "vendorController"
        })
        .when("/approve/:step/:requisitionId", {
            templateUrl : "views/rfr/approve.html",
            controller : "controllers.rfrController"
        })
        .when("/rfr/:step", {
            templateUrl : "views/rfr/requisition.html",
            controller : "controllers.rfrController"
        })
        .when("/rfr/:step/:requisitionId", {
            templateUrl : "views/rfr/requisition.html",
            controller : "controllers.rfrController"
        })
        .when("/", {
            templateUrl : "views/default.html",
            controller : "controllers.rfrController"
        })
		.when("/details/:step/:requisitionId", {

            templateUrl : "views/rfr/details.html",
            controller : "controllers.rfrController"
        })
        .when("/job/:step/:requisitionId", {
            templateUrl : "views/rfr/job.html",
            controller : "controllers.rfrController"
        })
        .when("/qualification/:step/:requisitionId", {
            templateUrl : "views/rfr/qualification.html",
            controller : "controllers.rfrController"
        })
        .when("/summary/:step/:requisitionId", {
            templateUrl : "views/rfr/summary.html",
            controller : "controllers.rfrController"
        })

		.when("/candidateProfile", {
            templateUrl : "views/candidate/profile.html",
            controller : "controllers.candidateController"
        })
		.when("/:step", {
            templateUrl : "views/rfr/requisition.html",
            controller : "controllers.rfrController"
        })
		.when("/:step/:requisitionId", {
            templateUrl : "views/rfr/requisition.html",
            controller : "controllers.rfrController"
        })

        .otherwise({
            template : "<h1>404</h1><p>Page Not Founnd</p>"
        });
    });