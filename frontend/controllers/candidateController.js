(function( ng, app ){

    "use strict";

    app.controller("controllers.candidateController",["$scope","$http","urlService","$location","$routeParams","commonFunctions","$rootScope","candidateApiServices","$window", "Upload", function($scope,$http,urlService,$location,$routeParams,commonFunctions,$rootScope,candidateApiServices,$window, Upload) {
        $scope.url = urlService.hosturl;
        $scope.profile = {};
        $scope.profile.otherDetail = {};
        $scope.profile.personalDetail = {};
        $scope.errorMessage = {};

        $scope.directoryPath = $rootScope.companyName + '' + $rootScope.folderSeparator+ '' +$rootScope.uploadResumeBy+ '' +$rootScope.folderSeparator+ '' +$rootScope.vendorCode+ '' +$rootScope.folderSeparator+ '' +$rootScope.jobID;

        $('#currentEmployerDurationMonthYearFrom, #currentEmployerDurationMonthYearTo').datepicker( {
            format: "yyyy-mm",
            viewMode: "years",
            minViewMode: 1,
            autoclose:true,
            closebutton: true,
            icons: {
                close: 'closeText'
            }
        });

        /*Search candidate profile*/
        $scope.searchCandidateProfile = function(){
            var isError = false;
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if(typeof $scope.searchCandidateProfileBox === "undefined" || $scope.searchCandidateProfileBox == '') {
                isError = true;
                $scope.errorMessage.searchCandidateProfile = "User name can't be empty.";
            } else if ( typeof $scope.searchCandidateProfileBox !== 'undefined' && reg.test($scope.searchCandidateProfileBox) === false ) {
                isError = true;
                $scope.errorMessage.searchCandidateProfile = "Invalid Username.";
            }

            if(!isError) {
                candidateApiServices.getCandidateDetail($scope.searchCandidateProfileBox, function(error, result){
                    if(error) {
                        alert(error)
                    } else {
                        var candidatedetailsSet = result.data.data.candidatedetails;
                        $scope.profile.otherDetail = candidatedetailsSet.otherDetail;
                        $scope.profile.personalDetail = candidatedetailsSet.personalDetail;
                        $scope.profile.employmentDetails = candidatedetailsSet.employmentDetails;
                        $scope.profile.educationalDetails = candidatedetailsSet.educationalDetails;
                    }
                });
            }
        }
        /*End search*/

        /*Resume upload*/
        $scope.uploadResume =  function(){
            if(typeof $scope.Imagefile !== "undefined" && $scope.Imagefile != '') {
                var requestData = {
                    "fileObject" : $scope.Imagefile,
                    "uploadPath" : $scope.directoryPath
                }
                candidateApiServices.resumeUploadAPIService(requestData, function(error, data){
                    if(error) {
                        alert("Error while uploading resume.");
                        console.log(error);
                    } else {
                        $scope.Imagefile = '';
                        candidateApiServices.resumeParseAPIService(data, function(error,data){
                            if(error) {
                                /*write code if resume parsed failed*/
                            } else {
                                /*write code if resume parsed successfully*/
                            }
                        });
                    }
                });
            } else {
                alert("Select Resume file.");
            }

        }

        /*Submit candidate profile start*/
        $scope.submitCandidateProfile = function(){

            if(!$scope.customError($scope.profile)) {
                candidateApiServices.candidate_create_update($scope.profile, function(error,data){
                    if(error) {
                        alert("Error while creating candidate profile.");
                    } else {
                        if(typeof data.candidateId !== "undefined" && typeof data.candidateId !== undefined && data.candidateId != "") {
                            candidateApiServices.candidatedetails_create_update(data.candidateId, $scope.profile, function(error,data){
                                if(error) {
                                    alert("Candidate profile successfully created but, getting error while submitting candidate detail.");
                                } else {
                                    if(typeof data.candidateDetailId !== "undefined" && typeof data.candidateDetailId !== undefined && data.candidateDetailId != "") {
                                        alert("Candidate profile & candidate detail successfully submitted.");
                                        $window.location.reload();
                                    }
                                }
                            });
                        }
                    }
                });
            } else {
                $window.scrollTo(0, angular.element(document.getElementById('candidateContainer')).offsetTop);
            }
        };

        $scope.hasError = function(field, validation){
            if(validation){
                return ($scope.candidateProfileForm[field].$dirty && $scope.candidateProfileForm[field].$error[validation]) || ($scope.submitted && $scope.candidateProfileForm[field].$error[validation]);
            }
            return ($scope.candidateProfileForm[field].$dirty && $scope.candidateProfileForm[field].$invalid) || ($scope.submitted && $scope.candidateProfileForm[field].$invalid);
        };

        $scope.customError = function(data){
            var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            var number = /^[0-9]+$/;
            var isError = false;
            //check validation rules
            if(typeof data.personalDetail.title === "undefined" || data.personalDetail.title == '') {
                isError = true;
                $scope.errorMessage.title = "Title is required.";
            }
            if(typeof data.personalDetail.firstName === "undefined" || data.personalDetail.firstName == '') {
                isError = true;
                $scope.errorMessage.firstName = "First name is required.";
            }
            if(typeof data.personalDetail.gender === "undefined" || data.personalDetail.gender == '') {
                isError = true;
                $scope.errorMessage.gender = "Gender name is required.";
            }
            if(typeof data.personalDetail.maritalStatus === "undefined" || data.personalDetail.maritalStatus == '') {
                isError = true;
                $scope.errorMessage.maritalStatus = "Marital status is required.";
            }
            if(typeof data.personalDetail.contactNo === "undefined" || data.personalDetail.contactNo == '') {
                isError = true;
                $scope.errorMessage.contactNo = "Contact no. is required.";
            }
            if(typeof data.personalDetail.personalEmailId === "undefined" || data.personalDetail.personalEmailId == '') {
                isError = true;
                $scope.errorMessage.personalEmailId = "Personal Email ID is required.";
            } else if ( typeof data.personalDetail.personalEmailId !== 'undefined' && reg.test(data.personalDetail.personalEmailId) === false ) {
                isError = true;
                $scope.errorMessage.personalEmailId = "Invalid Email Address";
            }
            if(typeof data.personalDetail.totalExperiance === "undefined" || data.personalDetail.totalExperiance == '') {
                isError = true;
                $scope.errorMessage.totalExperiance = "Total experiance is required.";
            }
            if(typeof data.personalDetail.country === "undefined" || data.personalDetail.country == '') {
                isError = true;
                $scope.errorMessage.country = "Country is required.";
            }
            if(typeof data.personalDetail.state === "undefined" || data.personalDetail.state == '') {
                isError = true;
                $scope.errorMessage.state = "State is required.";
            }
            if(typeof data.personalDetail.city === "undefined" || data.personalDetail.city == '') {
                isError = true;
                $scope.errorMessage.city = "City is required.";
            }
            if(typeof data.personalDetail.pincode === "undefined" || data.personalDetail.pincode == '') {
                isError = true;
                $scope.errorMessage.pincode = "Pincode is required.";
            }else if(!number.test(data.personalDetail.pincode)){
                    isError = true;
                    $scope.errorMessage.pincode = "Pincode should be Number";
            }

            return isError;
        };
        /*Submit candidate profile end*/

        /*Employment details detail start*/
        $scope.profile.employmentDetails = [];
        $scope.addEmploymentDetails = function() {
            $scope.profile.employmentDetails.push({
                currentEmployer: "",
                currentEmployerDesignation: "",
                currentEmployerJobProfile: "",
                currentEmployerDurationMonthYearTo: "",
                currentEmployerDurationMonthYearFrom : ""
            });
        };
        $scope.addEmploymentDetails();
        /*Employment details detail end*/

        /*Educational Details start*/
        $scope.profile.educationalDetails = [];
        $scope.addEducationDetails = function() {
            $scope.profile.educationalDetails.push({
                highestQualification: "",
                course: "",
                universityCollege: "",
                courseType: "",
                passingYear : ""
            });
        };
        $scope.addEducationDetails();
        /*Educational Details end*/

    }]);

})( angular, app );