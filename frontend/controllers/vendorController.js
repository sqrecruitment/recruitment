//vendor.js
/*
 * Programmed by Akarsh Nagariya
 * Recruitment Vendors
 *
 * */

angular
    .module('app')

    .controller('vendorController',['$q','$scope','$http','$rootScope','$location','urlService','$routeParams',function($q,$scope,$http,$rootScope,$location,urlService,$routeParams){
        $scope.formdata = {}
        $scope.error = {}
        $scope.location = {}
        $scope.setFortoken = false
        $scope.fetchMsg = false

        $scope.fetchAllVendors = function(){

            $http({
                method : "POST",
                url : urlService.apiurl+'vendorsMasters/getAllVendors',
                data : {"auth_data" :{}},
            }).then(function mySuccess(response) {

                $scope.tableHeads = ['Id','Vendor Name','Vendor Code','COMP-ID','Email','Address Details','Pin Code','Status','Action']
                $scope.FetchAllVendorsInTable = response.data.data
            })
        }


        $scope.fetchVendorCreate =function(){



            //Fetch Access Token For Vendor
            var takeurl = "dev"
            $http({
                method : "POST",
                url : urlService.apiurl+'apiMasters/getAccessToken',
                data : {"id" :takeurl},
            }).then(function mySuccess(response) {
                console.log(response.data.data)
                $scope.takeAccessToken = response.data.data
                var takejson = {"id":response.data.data.userId,"action":"country"}
                $http({
                    method : "GET",
                    url : urlService.apiurl+'apiMasters/getCompData?takeData='+JSON.stringify(takejson)+'&access_token='+response.data.data.id,
                }).then(function mySuccess(response1) {
                    $scope.takeAllCountry = response1.data.data

                    if($routeParams.vendorId !='add'){
                        $scope.fetchMsg = true
                        $scope.setFortoken = true

                        $http({
                            method : "POST",
                            url : urlService.apiurl+'vendorsMasters/getVendorAccessToken',
                            data : {"id" :$routeParams.vendorId},
                        }).then(function mySuccess(response) {
                            $scope.vendorAccessToken = response.data.data
                            $http({
                                method : "GET",
                                url : urlService.apiurl+'vendorsMasters/'+$scope.vendorAccessToken.userId+'?access_token='+$scope.vendorAccessToken.id,
                            }).then(function mySuccess(responseVendor) {
                                console.log(responseVendor.data)


                                $scope.fetchState(responseVendor.data.countryId,function(data,err){
                                    if(err){
                                        console.log(err)
                                    }
                                    else
                                    {
                                        $scope.fetchCity(responseVendor.data.stateId, function (data,err) {
                                            if(err){
                                                console.log(err)
                                            }
                                            else{
                                                console.log(data)
                                                $scope.formdata = responseVendor.data

                                                $scope.fetchMsg = false
                                            }
                                        })

                                    }
                                })




                            })
                        })
                    }

                })


            })
        }

        $scope.changeStatus = function(id,index){
            if($scope.FetchAllVendorsInTable[index].active)
                $scope.FetchAllVendorsInTable[index].active = false
            else
                $scope.FetchAllVendorsInTable[index].active = true
            $http({
                method : "POST",
                url : urlService.apiurl+'vendorsMasters/getVendorAccessToken',
                data : {"id" :id},
            }).then(function mySuccess(response) {
                    $http({
                        method : "PATCH",
                        url : urlService.apiurl + 'vendorsMasters/' + $scope.FetchAllVendorsInTable[index].id + '?access_token=' +response.data.data.id,
                        data : $scope.FetchAllVendorsInTable[index]
                    }).then(function mySuccess(response) {
                        if(response.status){
                            $scope.statusMsg = {"active":true,"msg":"Status Changed Successfully"}
                        }
                    })
            })
        }
        $scope.fetchState = function(takeCountry,callback){
            $scope.location.country = takeCountry
            if(takeCountry !== null){
                var takejson = {"id": $scope.takeAccessToken.userId,"action":"state","countryId":takeCountry}
                $http({
                    method : "GET",
                    url : urlService.apiurl+'apiMasters/getCompData?takeData='+JSON.stringify(takejson)+'&access_token='+ $scope.takeAccessToken.id,
                }).then(function mySuccess(response1) {
                    $scope.takeAllState = response1.data.data
                    if($scope.setFortoken)
                        callback($scope.takeAllState)
                })
            }
        }
        $scope.fetchCity = function(takeState,callback){

            if($scope.location.country !== null && takeState !== null){
                var takejson = {"id": $scope.takeAccessToken.userId,"action":"city","countryId":$scope.location.country,"stateId":takeState}
                $http({
                    method : "GET",
                    url : urlService.apiurl+'apiMasters/getCompData?takeData='+JSON.stringify(takejson)+'&access_token='+ $scope.takeAccessToken.id,
                }).then(function mySuccess(response1) {
                    $scope.takeAllCity = response1.data.data
                    if($scope.setFortoken)
                        callback($scope.takeAllCity)
                })
            }
        }

        $scope.createVendor = function(){
            $scope.formdata.passwordDecoded =  $scope.formdata.password
            $scope.formdata.username =  $scope.formdata.emailId
            $scope.formdata.startDate = new Date()
            $scope.formdata.active = true
            $scope.formdata.email = $scope.formdata.emailId
            if(!$scope.requisitionFormValidate($scope.formdata)) { return false; }
            if(typeof $scope.formdata.id !=='undefined'){
                $http({
                    method : "PATCH",
                    url : urlService.apiurl+'vendorsMasters/'+$scope.formdata.id+'?access_token='+$scope.vendorAccessToken.id,
                    data : $scope.formdata
                }).then(function mySuccess(response) {
                    if(response.status){
                        window.location = '#!/vendors'
                    }
                })

            }
            if(typeof $scope.formdata.id ==='undefined'){
                $http({
                    method : "POST",
                    url : urlService.apiurl+'vendorsMasters',
                    data : $scope.formdata,

                }).then(function mySuccess(response) {
                    if(response.status){
                        window.location = '#!/vendors'
                    }
                })

            }


            /*$scope.checkDataErrors = {}
            $scope.checkDataErrors.companyId = false
            */
        }
        $scope.requisitionFormValidate = function(data){
            var f = 0;
            $scope.error = {};

            if(typeof data.vendorCode === 'undefined'){
                f = 1;
                $scope.error.vendorCode = "Vendor Code is required";
            }
            if(typeof data.vendorName === 'undefined'){
                f = 1;
                $scope.error.vendorName = "Vendor Name is required";
            }
            if(typeof data.addressDetails === 'undefined'){
                f = 1;
                $scope.error.addressDetails = "Address Details is required";
            }
            if(typeof data.pinCode === 'undefined'){
                f = 1;
                $scope.error.pinCode = "Pincode is required";
            }
            else{
                var number = /^[0-9]+$/
                if(!number.test(data.pinCode)){
                    f = 1;
                    $scope.error.pinCode = "Pincode should be Number";
                }

            }
                var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

                if ( typeof data.emailId != 'undefined' && reg.test(data.emailId) == false )
                {
                    f = 1;
                    $scope.error.emailId = "Invalid Email Address";

                }
            if ( typeof data.emailId === 'undefined'){
                    f = 1;
                    $scope.error.emailId = "Email Id is required";
                }
            if(typeof data.password === 'undefined'){
                f = 1;
                $scope.error.password = "Password is required";
            }



            return (f == 0) ? true : false;
        }

    }])


