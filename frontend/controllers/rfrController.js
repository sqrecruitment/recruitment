(function( ng, app ){

    "use strict";

    app.controller("controllers.rfrController",function($scope,$http,urlService,$location,$routeParams,commonFunctions,errorStrings,$rootScope, rfrApiServices) {
        //console.log(urlService);
        //alert($scope.authToken);
        $scope.step = $routeParams.step;
		$scope.url = urlService.hosturl;
        var empCode = "SMS002";


        ////////////////////////////////////// RFR (requisition) Step 1 Starts ///////////////////////////////////////////
        if($routeParams.step == 1)
        {
            if(typeof $routeParams.requisitionId !== 'undefined')
            {
				rfrApiServices.requisitions_getData($routeParams.requisitionId, function(error,data){
					if(error) {
						alert("Something went wrong");
					} else {
						$scope.requisition = data;
					}
				});
            }
            else
            {
                $scope.requisition = {"status" :  1,"requisitionId" : "0"};
            }
        }

        $scope.submitRequisitionForm = function () {
            var data = {
                "title": $scope.requisition.title,
                "positionType": $scope.requisition.positionType,
                "other": $scope.requisition.other,
                "id" : $scope.requisition.requisitionId,
                "status": $scope.requisition.status
            };
            
            if(!$scope.requisitionFormValidate(data)) { return false; }

            rfrApiServices.requisitions_saveData(data, function(error,data){
                if(error) {
                    alert("Something went wrong");
                } else {
                    $location.path('/details/2/'+data.id);
                }
            });

        };
         
        $scope.resetRequisitionForm = function () {
            $scope.requisition = {};
        };

        $scope.requisitionFormValidate = function(data){
            var f = 0;
            $scope.error = {};
            
            if(typeof data.title === 'undefined'){
                f = 1;
                $scope.error.title_err = errorStrings.required;
            }
            if(typeof data.positionType === 'undefined'){
                f = 1;
                $scope.error.positionType_err = errorStrings.required;
            }

            return (f == 0) ? true : false;
        }

        ////////////////////////////////////// RFR (requisition) Step 1 Ends /////////////////////////////////////////////

        ////////////////////////////////////// RFR (details) Step 2 Starts /////////////////////////////////////////////
        if($routeParams.step == 2)
        {
            $scope.grades = [
                                {"value" : "0","name" : "Select Grade"},
                                {"value":"1","name":"A"},
                                {"value":"2","name":"B"},
                                {"value":"3","name":"C"}
                            ];

            $scope.businessUnits = [
                                        {"value" : "0","name" : "Select Business Unit"},
                                        {"value":"1","name":"BU1"},
                                        {"value":"2","name":"BU2"},
                                        {"value":"3","name":"BU3"}
                                   ];

            $scope.gradeLevel = [
                                    {"value" : "0","name" : "Select Grade Level"},
                                    {"value":"1","name":"L1"},
                                    {"value":"2","name":"L2"},
                                    {"value":"3","name":"L3"}
                                ];

            $scope.criticality = [
                                    {"value" : "0","name" : "Select Criticality"},
                                    {"value":"1","name":"C1"},
                                    {"value":"2","name":"C2"},
                                    {"value":"3","name":"C3"}
                                ];

            $scope.roles =  [
                                {"value" : "0","name" : "Select Role"},
                                {"value":"1","name":"Role1"},
                                {"value":"2","name":"Role2"},
                                {"value":"3","name":"Role3"}
                            ];

            $scope.recruitmentStages =  [
                                            {"value" : "0","name" : "Select Stage"},
                                            {"value":"1","name":"Stage1"},
                                            {"value":"2","name":"Stage2"},
                                            {"value":"3","name":"Stage3"}
                                        ];


            $scope.levels = [{"srNo" : 1,"level" : 1,"role" : "0","empCode" : "0","status" : 0}];
            $scope.stages = [{"srNo" : 1,"stage" : 1,"stageId" : "0","empCode" : "0","status" : 0}];

            $scope.formdata =   {
                                    "requisitionId" :  $routeParams.requisitionId,
                                    "status" : 1,
                                    "grade" : "0",
                                    "gradeLevel" : "0",
                                    "businessUnit" : "0",
                                    "criticality" : "0",
                                    "role" : "0",
                                    "company" : "abc",
                                    "companyId" : 1,
                                    "id" : "0"
                                };

            $http({
                method : "POST",
                url : urlService.apiurl+'RequisitionDetails/getData',
                data : {"requisitionId" :$routeParams.requisitionId},
            }).then(function mySuccess(response) {
                //console.log(response.data.data);
                var alldata = response.data.data;
                if(alldata)
                {
                    
                    $scope.formdata = {
                                        "id" : alldata.id,
                                        "requisitionId" : alldata.requisitionId,
                                        "requestNo": alldata.requestNo,
                                        "requesterName": alldata.requesterName,
                                        "company": "abc",
                                        "companyId": 1,
                                        "businessUnit": alldata.businessUnit,
                                        "grade": alldata.grade,
                                        "gradeLevel": alldata.gradeLevel,
                                        "openings": alldata.openings,
                                        "hiringType": alldata.hiringType,
                                        "replacementFor": alldata.replacementFor,
                                        "criticality": alldata.criticality,
                                        "isBudgeted": alldata.isBudgeted,
                                        "maxBudget": alldata.maxBudget,
                                        "minBudget": alldata.minBudget,
                                        "status": alldata.status
                                    };
                    $scope.formdata.requestDate = new Date(alldata.requestDate);
                    $scope.formdata.closingDate = new Date(alldata.closingDate);
                    $scope.formdata.startingDate = new Date(alldata.startingDate);
                    $scope.formdata.targetHireDate = new Date(alldata.targetHireDate);
                    $scope.formdata.targetEndDate = new Date(alldata.targetEndDate);
                    $scope.levels = alldata.levels;
                    $scope.stages = alldata.stages;
                }
                
            }, function myError(response) {
                console.log(response);
            });
        }

        $scope.detailsAddLevel = function(){
            if($scope.levels.length < $rootScope.approvalLevels){
                $scope.levels.push({"srNo" : $scope.levels.length + 1,"level" : $scope.levels.length + 1,"role" : "0","empCode" : "0","status" : 0});   
            }
            else{
                $scope.levelAdd = true;
            }
        };

        $scope.detailsAddStage = function(){
            if($scope.stages.length < $rootScope.recruitmentStages){
                $scope.stages.push({"srNo" : $scope.stages.length + 1,"stage" : $scope.stages.length + 1,"stageId" : "0","empCode" : "0","status" : 0});   
            }
            else{
                $scope.stageAdd = true;
            }
        };

        $scope.getRoleEmployees = function(roleId,index){

           var data = [
                            {"value" : "0","name" : "Select Employee"},
                            {"value":"SMS001","name":"emp1"},
                            {"value":"SMS002","name":"emp2"},
                            {"value":"SMS003","name":"emp3"}
                        ];
            return data; 
        };

        $scope.getStageEmployees = function(stageId,index){

           var data = [
                            {"value" : "0","name" : "Select Employee"},
                            {"value":"SMS001","name":"emp1"},
                            {"value":"SMS002","name":"emp2"},
                            {"value":"SMS003","name":"emp3"}
                        ];
            return data; 
        };

        $scope.submitDetailsForm = function () {
            var data = {
                "id" : $scope.formdata.id,
                "requisitionId" : $scope.formdata.requisitionId,
                "requestNo": $scope.formdata.requestNo,
                "requestDate": $scope.formdata.requestDate,
                "requesterName": $scope.formdata.requesterName,
                "company": $scope.formdata.companyId,
                "businessUnit": $scope.formdata.businessUnit,
                "grade": $scope.formdata.grade,
                "gradeLevel": $scope.formdata.gradeLevel,
                "openings": $scope.formdata.openings,
                "hiringType": $scope.formdata.hiringType,
                "replacementFor": $scope.formdata.replacementFor,
                "criticality": $scope.formdata.criticality,
                "closingDate": $scope.formdata.closingDate,
                "startingDate": $scope.formdata.startingDate,
                "targetHireDate": $scope.formdata.targetHireDate,
                "targetEndDate": $scope.formdata.targetEndDate,
                "isBudgeted": $scope.formdata.isBudgeted,
                "maxBudget": $scope.formdata.maxBudget,
                "minBudget": $scope.formdata.minBudget,
                "levels": $scope.levels,
                "stages": $scope.stages,
                "status": $scope.formdata.status
            };
            
            if(!$scope.detailsFormValidate(data)) { return false; }

            $http({
                method : "POST",
                url : urlService.apiurl+'RequisitionDetails/saveData',
                data : {'data':data},
            }).then(function mySuccess(response) {
                console.log(response.data.data);
                console.log(response.data.data.id);
                $location.path('/job/3/'+$scope.formdata.requisitionId);
            }, function myError(response) {
                console.log(response);
            });

        };

        $scope.detailsGoback = function () {
            $location.path('/rfr/1/'+$routeParams.requisitionId);
        };

        $scope.detailsFormValidate = function(data){
            var f = 0;
            $scope.error = {};
            
            if(typeof data.requestNo === 'undefined'){
                f = 1;
                $scope.error.requestNo_err = errorStrings.required;
            }
            if(!data.requestDate){
                f = 1;
                $scope.error.requestDate_err = errorStrings.required;
            }
            if(typeof data.requesterName === 'undefined'){
                f = 1;
                $scope.error.requesterName_err = errorStrings.required;
            }
            if(data.businessUnit == '0'){
                f = 1;
                $scope.error.businessUnit_err = errorStrings.required;
            }
            if(data.grade == '0'){
                f = 1;
                $scope.error.grade_err = errorStrings.required;
            }
            if(data.gradeLevel == '0'){
                f = 1;
                $scope.error.gradeLevel_err = errorStrings.required;
            }
            if(typeof data.openings === 'undefined'){
                f = 1;
                $scope.error.openings_err = errorStrings.required;
            }
            if(typeof data.hiringType === 'undefined'){
                f = 1;
                $scope.error.hiringType_err = errorStrings.required;
            }
            if(data.criticality == '0'){
                f = 1;
                $scope.error.criticality_err = errorStrings.required;
            }
            if(!data.closingDate){
                f = 1;
                $scope.error.closingDate_err = errorStrings.required;
            }
            if(!data.startingDate){
                f = 1;
                $scope.error.startingDate_err = errorStrings.required;
            }
            if(!data.targetHireDate){
                f = 1;
                $scope.error.targetHireDate_err = errorStrings.required;
            }
            if(!data.targetEndDate){
                f = 1;
                $scope.error.targetEndDate_err = errorStrings.required;
            }
            if(typeof data.isBudgeted === 'undefined'){
                f = 1;
                $scope.error.isBudgeted_err = errorStrings.required;
            }
            if(typeof data.minBudget === 'undefined'){
                f = 1;
                $scope.error.minBudget_err = errorStrings.required;
            }
            if(typeof data.maxBudget === 'undefined'){
                f = 1;
                $scope.error.maxBudget_err = errorStrings.required;
            }
            return (f == 0) ? true : false;
        }
        ////////////////////////////////////// RFR (details) Step 2 Ends /////////////////////////////////////////////

        ////////////////////////////////////// RFR (job) Step 3 Starts /////////////////////////////////////////////
        if($routeParams.step == 3)
        {
            $scope.locations = [
                            {"value" : "0","name" : "Select Location"},
                            {"value":"1","name":"L1"},
                            {"value":"2","name":"L2"},
                            {"value":"3","name":"L3"}
                        ];
            $http({
                method : "POST",
                url : urlService.apiurl+'RequisitionJobDetails/getData',
                data : {"requisitionId" :$routeParams.requisitionId},
            }).then(function mySuccess(response) {
                //console.log(response.data.data);
                var alldata = response.data.data;
                if(alldata)
                {
                    $scope.formdata = {
                                        "id" : alldata.id,
                                        "requisitionId" : alldata.requisitionId,
                                        "position": alldata.position,
                                        "location": alldata.location,
                                        "description": alldata.description,
                                        "skills": alldata.skills,
                                        "others": alldata.others,
                                        "type": alldata.type,
                                        "status": alldata.status
                                    };
                }
                else
                {
                    $scope.formdata = {
                                "requisitionId" :  $routeParams.requisitionId,
                                "status" : 1,
                                "location" : "0",
                                "id" : "0"
                            };
                } 
                
            }, function myError(response) {
                console.log(response);
            });
        }

        $scope.submitJobForm = function () {
            var data = {
                "id" : $scope.formdata.id,
                "requisitionId" : $scope.formdata.requisitionId,
                "position": $scope.formdata.position,
                "location": $scope.formdata.location,
                "description": $scope.formdata.description,
                "skills": $scope.formdata.skills,
                "others": $scope.formdata.others,
                "type": $scope.formdata.type,
                "status": $scope.formdata.status
            };

            if(!$scope.jobFormValidate(data)) { return false; }

            $http({
                method : "POST",
                url : urlService.apiurl+'RequisitionJobDetails/saveData',
                data : {'data':data},
            }).then(function mySuccess(response) {
                console.log(response.data.data);
                $location.path('/qualification/4/'+$scope.formdata.requisitionId);
            }, function myError(response) {
                console.log(response);
            });

        };
 
        $scope.jobGoBack = function () {
            $location.path('/details/2/'+$routeParams.requisitionId);
        };

        $scope.jobFormValidate = function(data){
            var f = 0;
            $scope.error = {};
            
            if(typeof data.position === 'undefined'){
                f = 1;
                $scope.error.position_err = errorStrings.required;
            }
            if(typeof data.location === 'undefined'){
                f = 1;
                $scope.error.location_err = errorStrings.required;
            }
            if(typeof data.description === 'undefined'){
                f = 1;
                $scope.error.description_err = errorStrings.required;
            }
            if(typeof data.skills === 'undefined'){
                f = 1;
                $scope.error.skills_err = errorStrings.required;
            }
            if(typeof data.others === 'undefined'){
                f = 1;
                $scope.error.others_err = errorStrings.required;
            }
            if(typeof data.type === 'undefined'){
                f = 1;
                $scope.error.type_err = errorStrings.required;
            }
            return (f == 0) ? true : false;
        }
        ////////////////////////////////////// RFR (job) Step 3 Ends /////////////////////////////////////////////

        ////////////////////////////////////// RFR (qualification) Step 4 Starts /////////////////////////////////////
        if($routeParams.step == 4)
        {   
            $http({
                method : "POST",
                url : urlService.apiurl+'RequisitionQualificationDetails/getData',
                data : {"requisitionId" :$routeParams.requisitionId},
            }).then(function mySuccess(response) {
                //console.log(response.data.data);
                var alldata = response.data.data;
                if(alldata)
                {
                    $scope.formdata = {
                                        "id" : alldata.id,
                                        "requisitionId" : alldata.requisitionId,
                                        "educationalQualification": alldata.educationalQualification,
                                        "experience": alldata.experience,
                                        "status": alldata.status
                                    };
                }
                else
                {
                    $scope.formdata = {
                                "requisitionId" :  $routeParams.requisitionId,
                                "status" : 1
                            };
                } 
                
            }, function myError(response) {
                console.log(response);
            });
        }

        $scope.submitQualificationForm = function () {
            var data = {
                "id" :  $scope.formdata.id,
                "requisitionId" : $scope.formdata.requisitionId,
                "educationalQualification": $scope.formdata.educationalQualification,
                "experience": $scope.formdata.experience,
                "status": $scope.formdata.status
            };

            if(!$scope.qualificationFormValidate(data)) { return false; }

            $http({
                method : "POST",
                url : urlService.apiurl+'RequisitionQualificationDetails/saveData',
                data : {'data':data},
            }).then(function mySuccess(response) {
                console.log(response.data.data);
                $location.path('/summary/5/'+$routeParams.requisitionId);
            }, function myError(response) {
                console.log(response);
            });
        };

        $scope.qualificationGoBack = function () {
            $location.path('/job/3/'+$routeParams.requisitionId);
        };

        $scope.qualificationFormValidate = function(data){
            var f = 0;
            $scope.error = {};
            
            if(typeof data.educationalQualification === 'undefined'){
                f = 1;
                $scope.error.educationalQualification_err = errorStrings.required;
            }
            if(typeof data.experience === 'undefined'){
                f = 1;
                $scope.error.experience_err = errorStrings.required;
            }

            return (f == 0) ? true : false;
        }
        ////////////////////////////////////// RFR (qualification) Step 4 Ends ///////////////////////////////////////

        ////////////////////////////////////// RFR (summary) Step 5 starts ///////////////////////////////////////
        if($routeParams.step == 5)
        {
            $http({
                method : "POST",
                url : urlService.apiurl+'Requisitions/getData',
                data : {"requisitionId" :$routeParams.requisitionId},
            }).then(function mySuccess(response) {
                //console.log(response.data.data);
                var alldata = response.data.data;
                if(alldata)
                {

                }
                else
                {
                    $location.path('/1/');    
                }
            }, function myError(response) {
                console.log(response);
            });

            $scope.locations = [
                                {"value" : "0","name" : "Select Location"},
                                {"value":"1","name":"L1"},
                                {"value":"2","name":"L2"},
                                {"value":"3","name":"L3"}
                            ];

            $scope.grades = [
                                {"value" : "0","name" : "Select Grade"},
                                {"value":"1","name":"A"},
                                {"value":"2","name":"B"},
                                {"value":"3","name":"C"}
                            ];

            $scope.businessUnits = [
                                {"value" : "0","name" : "Select Business Unit"},
                                {"value":"1","name":"BU1"},
                                {"value":"2","name":"BU2"},
                                {"value":"3","name":"BU3"}
                            ];

            $scope.gradeLevels = [
                                {"value" : "0","name" : "Select Grade Level"},
                                {"value":"1","name":"L1"},
                                {"value":"2","name":"L2"},
                                {"value":"3","name":"L3"}
                            ];

            $scope.criticality = [
                                {"value" : "0","name" : "Select Criticality"},
                                {"value":"1","name":"C1"},
                                {"value":"2","name":"C2"},
                                {"value":"3","name":"C3"}
                            ];

            $scope.roles =  [
                                {"value" : "0","name" : "Select Role"},
                                {"value":"1","name":"Role1"},
                                {"value":"2","name":"Role2"},
                                {"value":"3","name":"Role3"}
                            ];

            $scope.recruitmentStages =  [
                                            {"value" : "0","name" : "Select Stage"},
                                            {"value":"1","name":"Stage1"},
                                            {"value":"2","name":"Stage2"},
                                            {"value":"3","name":"Stage3"}
                                        ];


            $scope.levels = [{"srNo" : 1,"level" : 1,"role" : "0","empCode" : "0","status" : 0}];
            $scope.stages = [{"srNo" : 1,"stage" : 1,"stageId" : "0","empCode" : "0","status" : 0}];

            //get details data start
            $http({
                method : "POST",
                url : urlService.apiurl+'RequisitionDetails/getData',
                data : {"requisitionId" :$routeParams.requisitionId},
            }).then(function mySuccess(response) {
                //console.log(response.data.data);
                var alldata = response.data.data;
                if(alldata)
                {
                    $scope.formdata = {
                                        "detailsId" : alldata.id,
                                        "requisitionId" : alldata.requisitionId,
                                        "requestNo": alldata.requestNo,
                                        "requesterName": alldata.requesterName,
                                        "company": "abc",
                                        "companyId": 1,
                                        "businessUnit": alldata.businessUnit,
                                        "grade": alldata.grade,
                                        "gradeLevel": alldata.gradeLevel,
                                        "openings": alldata.openings,
                                        "hiringType": alldata.hiringType,
                                        "replacementFor": alldata.replacementFor,
                                        "criticality": alldata.criticality,
                                        "isBudgeted": alldata.isBudgeted,
                                        "maxBudget": alldata.maxBudget,
                                        "minBudget": alldata.minBudget,
                                        "status": alldata.status
                                    };
                    $scope.formdata.requestDate = new Date(alldata.requestDate);
                    $scope.formdata.closingDate = new Date(alldata.closingDate);
                    $scope.formdata.startingDate = new Date(alldata.startingDate);
                    $scope.formdata.targetHireDate = new Date(alldata.targetHireDate);
                    $scope.formdata.targetEndDate = new Date(alldata.targetEndDate);
                    $scope.levels = alldata.levels;
                    $scope.stages = alldata.stages;
                }
                else
                {
                    $location.path('/details/2/'+$scope.formdata.requisitionId);
                } 
                
            }, function myError(response) {
                console.log(response);
            });
            //get details data end
            
            //get jobdata start
            $http({
                method : "POST",
                url : urlService.apiurl+'RequisitionJobDetails/getData',
                data : {"requisitionId" :$routeParams.requisitionId},
            }).then(function mySuccess(response) {
                //console.log(response.data.data);
                var alldata = response.data.data;
                if(alldata)
                {
                    $scope.formdata.jobId = alldata.id;
                    $scope.formdata.position = alldata.position;
                    $scope.formdata.description = alldata.description;
                    $scope.formdata.location = alldata.location;
                    $scope.formdata.skills = alldata.skills;
                    $scope.formdata.others = alldata.others;
                    $scope.formdata.type = alldata.type;
                }
                else
                {
                    $location.path('/job/3/'+$scope.formdata.requisitionId);
                } 
            }, function myError(response) {
                console.log(response);
            });
            //get jobdata end

            //get qualification data start
            $http({
                method : "POST",
                url : urlService.apiurl+'RequisitionQualificationDetails/getData',
                data : {"requisitionId" :$routeParams.requisitionId},
            }).then(function mySuccess(response) {
                //console.log(response.data.data);
                var alldata = response.data.data;
                if(alldata)
                {
                    $scope.formdata.qualificationId = alldata.id;
                    $scope.formdata.educationalQualification = alldata.educationalQualification;
                    $scope.formdata.experience = alldata.experience;
                }
                else
                {
                    $location.path('/qualification/4/'+$scope.formdata.requisitionId);
                } 
                
            }, function myError(response) {
                console.log(response);
            });
        }

        $scope.summaryAddLevel = function(){
            if($scope.levels.length < $rootScope.approvalLevels){
                $scope.levels.push({"srNo" : $scope.levels.length + 1,"level" : $scope.levels.length + 1,"role" : "0","empCode" : "0"});   
            }
            else{
                $scope.levelAdd = true;
            }
        };

        $scope.summaryAddStage = function(){
            if($scope.stages.length < $rootScope.recruitmentStages){
                $scope.stages.push({"srNo" : $scope.stages.length + 1,"stage" : $scope.stages.length + 1,"stageId" : "0","empCode" : "0"});   
            }
            else{
                $scope.stageAdd = true;
            }
        };

        $scope.submitSummaryForm = function (status) {
            $scope.formdata.status = status;
            var data = {
                "id" : $scope.formdata.detailsId,
                "requisitionId" : $scope.formdata.requisitionId,
                "requestNo": $scope.formdata.requestNo,
                "requestDate": $scope.formdata.requestDate,
                "requesterName": $scope.formdata.requesterName,
                "company": $scope.formdata.companyId,
                "businessUnit": $scope.formdata.businessUnit,
                "grade": $scope.formdata.grade,
                "gradeLevel": $scope.formdata.gradeLevel,
                "openings": $scope.formdata.openings,
                "hiringType": $scope.formdata.hiringType,
                "replacementFor": $scope.formdata.replacementFor,
                "criticality": $scope.formdata.criticality,
                "closingDate": $scope.formdata.closingDate,
                "startingDate": $scope.formdata.startingDate,
                "targetHireDate": $scope.formdata.targetHireDate,
                "targetEndDate": $scope.formdata.targetEndDate,
                "isBudgeted": $scope.formdata.isBudgeted,
                "maxBudget": $scope.formdata.maxBudget,
                "minBudget": $scope.formdata.minBudget,
                "levels": $scope.levels,
                "stages": $scope.stages,
                "status": 1
            };

            if(!$scope.summaryFormValidate($scope.formdata)) { return false; }

            $http({
                method : "POST",
                url : urlService.apiurl+'RequisitionDetails/saveData',
                data : {'data':data},
            }).then(function mySuccess(response) {
                $scope.jobFunction();
            }, function myError(response) {
                console.log(response);
                $location.path('/rfr/1/'+$scope.formdata.requisitionId);
            });
        };

        $scope.updateRequisitionStatus = function(){

            var data = {'requisitionId':$scope.formdata.requisitionId,"status" : $scope.formdata.status,"levels" : $scope.levels,"hosturl" : urlService.hosturl};
            rfrApiServices.requisitions_updateStatus(data, function(error,data){
                if(error) {
                    alert("Something went wrong");
                } else {
                    alert("RFR created successfully");
                    $location.path('/');
                }
            });
        };

        $scope.qualificationFunction = function(){
            var qualificationdata = {
                "id" :  $scope.formdata.qualificationId,
                "requisitionId" : $scope.formdata.requisitionId,
                "educationalQualification": $scope.formdata.educationalQualification,
                "experience": $scope.formdata.experience,
                "status": 1
            };

            $http({
                method : "POST",
                url : urlService.apiurl+'RequisitionQualificationDetails/saveData',
                data : {'data':qualificationdata},
            }).then(function mySuccess(response) {
                $scope.updateRequisitionStatus();
            }, function myError(response) {
                console.log(response);
            });
        };

        //This will be called after the deails function  
        $scope.jobFunction = function () {
            var jobdata = {
                "id" : $scope.formdata.jobId,
                "requisitionId" : $scope.formdata.requisitionId,
                "position": $scope.formdata.position,
                "location": $scope.formdata.location,
                "description": $scope.formdata.description,
                "skills": $scope.formdata.skills,
                "others": $scope.formdata.others,
                "type": $scope.formdata.type,
                "status": 1
            };

            $http({
                method : "POST",
                url : urlService.apiurl+'RequisitionJobDetails/saveData',
                data : {'data':jobdata},
            }).then(function mySuccess(response) {
                $scope.qualificationFunction();
            }, function myError(response) {
                $location.path('/details/2/'+$scope.formdata.requisitionId);
                console.log(response);
            });
        };

        //goback function. This will be called on back button click.  
        $scope.summaryGoBack = function () {
            $location.path('/qualification/4/'+$routeParams.requisitionId);
        };

        $scope.summaryFormValidate = function(data){
            var f = 0;
            $scope.error = {};
            
            if(typeof data.requestNo === 'undefined'){
                f = 1;
                $scope.error.requestNo_err = errorStrings.required;
            }
            if(!data.requestDate){
                f = 1;
                $scope.error.requestDate_err = errorStrings.required;
            }
            if(typeof data.requesterName === 'undefined'){
                f = 1;
                $scope.error.requesterName_err = errorStrings.required;
            }
            if(data.businessUnit == '0'){
                f = 1;
                $scope.error.businessUnit_err = errorStrings.required;
            }
            if(data.grade == '0'){
                f = 1;
                $scope.error.grade_err = errorStrings.required;
            }
            if(data.gradeLevel == '0'){
                f = 1;
                $scope.error.gradeLevel_err = errorStrings.required;
            }
            if(typeof data.openings === 'undefined'){
                f = 1;
                $scope.error.openings_err = errorStrings.required;
            }
            if(typeof data.hiringType === 'undefined'){
                f = 1;
                $scope.error.hiringType_err = errorStrings.required;
            }
            if(data.criticality == '0'){
                f = 1;
                $scope.error.criticality_err = errorStrings.required;
            }
            if(!data.closingDate){
                f = 1;
                $scope.error.closingDate_err = errorStrings.required;
            }
            if(!data.startingDate){
                f = 1;
                $scope.error.startingDate_err = errorStrings.required;
            }
            if(!data.targetHireDate){
                f = 1;
                $scope.error.targetHireDate_err = errorStrings.required;
            }
            if(!data.targetEndDate){
                f = 1;
                $scope.error.targetEndDate_err = errorStrings.required;
            }
            if(typeof data.isBudgeted === 'undefined'){
                f = 1;
                $scope.error.isBudgeted_err = errorStrings.required;
            }
            if(typeof data.minBudget === 'undefined'){
                f = 1;
                $scope.error.minBudget_err = errorStrings.required;
            }
            if(typeof data.maxBudget === 'undefined'){
                f = 1;
                $scope.error.maxBudget_err = errorStrings.required;
            }
            if(typeof data.position === 'undefined'){
                f = 1;
                $scope.error.position_err = errorStrings.required;
            }
            if(typeof data.location === 'undefined'){
                f = 1;
                $scope.error.location_err = errorStrings.required;
            }
            if(typeof data.description === 'undefined'){
                f = 1;
                $scope.error.description_err = errorStrings.required;
            }
            if(typeof data.skills === 'undefined'){
                f = 1;
                $scope.error.skills_err = errorStrings.required;
            }
            if(typeof data.others === 'undefined'){
                f = 1;
                $scope.error.others_err = errorStrings.required;
            }
            if(typeof data.type === 'undefined'){
                f = 1;
                $scope.error.type_err = errorStrings.required;
            }
            if(typeof data.educationalQualification === 'undefined'){
                f = 1;
                $scope.error.educationalQualification_err = errorStrings.required;
            }
            if(typeof data.experience === 'undefined'){
                f = 1;
                $scope.error.experience_err = errorStrings.required;
            }
            return (f == 0) ? true : false;
        }
        ////////////////////////////////////// RFR (summary) Step 5 Ends ///////////////////////////////////////


        ////////////////////////////////////// RFR (approval) starts ///////////////////////////////////////////
        if($routeParams.step == 6)
        {
            $scope.message = false;
            $scope.mainContainer = true;
            $scope.remarksDiv = false;

            $scope.approvalRemarks = [];
            $http({
                method : "POST",
                url : urlService.apiurl+'Requisitions/getData',
                data : {"requisitionId" :$routeParams.requisitionId},
            }).then(function mySuccess(response) {
                //console.log(response.data.data);
                var alldata = response.data.data;
                if(alldata)
                {

                }
                else
                {
                    $location.path('/rfr/1/');    
                }
            }, function myError(response) {
                console.log(response);
            });

            $scope.locations = [
                                {"value" : "0","name" : "Select Location"},
                                {"value":"1","name":"L1"},
                                {"value":"2","name":"L2"},
                                {"value":"3","name":"L3"}
                            ];

            $scope.grades = [
                                {"value" : "0","name" : "Select Grade"},
                                {"value":"1","name":"A"},
                                {"value":"2","name":"B"},
                                {"value":"3","name":"C"}
                            ];

            $scope.businessUnits = [
                                {"value" : "0","name" : "Select Business Unit"},
                                {"value":"1","name":"BU1"},
                                {"value":"2","name":"BU2"},
                                {"value":"3","name":"BU3"}
                            ];

            $scope.gradeLevels = [
                                {"value" : "0","name" : "Select Grade Level"},
                                {"value":"1","name":"L1"},
                                {"value":"2","name":"L2"},
                                {"value":"3","name":"L3"}
                            ];

            $scope.criticality = [
                                {"value" : "0","name" : "Select Criticality"},
                                {"value":"1","name":"C1"},
                                {"value":"2","name":"C2"},
                                {"value":"3","name":"C3"}
                            ];

            $scope.roles =  [
                                {"value" : "0","name" : "Select Role"},
                                {"value":"1","name":"Role1"},
                                {"value":"2","name":"Role2"},
                                {"value":"3","name":"Role3"}
                            ];

            $scope.recruitmentStages =  [
                                            {"value" : "0","name" : "Select Stage"},
                                            {"value":"1","name":"Stage1"},
                                            {"value":"2","name":"Stage2"},
                                            {"value":"3","name":"Stage3"}
                                        ];


            $scope.levels = [{"srNo" : 1,"level" : 1,"role" : "0","empCode" : "0","status" : 0}];
            $scope.stages = [{"srNo" : 1,"stage" : 1,"stageId" : "0","empCode" : "0","status" : 0}];
           

            //get details data start
            $http({
                method : "POST",
                url : urlService.apiurl+'RequisitionDetails/getData',
                data : {"requisitionId" :$routeParams.requisitionId},
            }).then(function mySuccess(response) {
                //console.log(response.data.data);
                var alldata = response.data.data;
                if(alldata)
                {
                    $scope.formdata = {
                                        "detailsId" : alldata.id,
                                        "requisitionId" : alldata.requisitionId,
                                        "requestNo": alldata.requestNo,
                                        "requesterName": alldata.requesterName,
                                        "company": "abc",
                                        "companyId": 1,
                                        "businessUnit": alldata.businessUnit,
                                        "grade": alldata.grade,
                                        "gradeLevel": alldata.gradeLevel,
                                        "openings": alldata.openings,
                                        "hiringType": alldata.hiringType,
                                        "replacementFor": alldata.replacementFor,
                                        "criticality": alldata.criticality,
                                        "isBudgeted": alldata.isBudgeted,
                                        "maxBudget": alldata.maxBudget,
                                        "minBudget": alldata.minBudget,
                                        "status": alldata.status
                                    };
                    $scope.formdata.requestDate = new Date(alldata.requestDate);
                    $scope.formdata.closingDate = new Date(alldata.closingDate);
                    $scope.formdata.startingDate = new Date(alldata.startingDate);
                    $scope.formdata.targetHireDate = new Date(alldata.targetHireDate);
                    $scope.formdata.targetEndDate = new Date(alldata.targetEndDate);
                    $scope.levels = alldata.levels;
                    $scope.stages = alldata.stages;
                    $scope.createdBy = alldata.createdBy;
                    $scope.formdata.remarks = '';
                    $scope.checkAccess($scope.levels);
                }
                else
                {
                    $location.path('/details/2/'+$scope.formdata.requisitionId);
                } 
                
            }, function myError(response) {
                console.log(response);
            });
            //get details data end
            
            //get jobdata start
            $http({
                method : "POST",
                url : urlService.apiurl+'RequisitionJobDetails/getData',
                data : {"requisitionId" :$routeParams.requisitionId},
            }).then(function mySuccess(response) {
                //console.log(response.data.data);
                var alldata = response.data.data;
                if(alldata)
                {
                    $scope.formdata.jobId = alldata.id;
                    $scope.formdata.position = alldata.position;
                    $scope.formdata.description = alldata.description;
                    $scope.formdata.location = alldata.location;
                    $scope.formdata.skills = alldata.skills;
                    $scope.formdata.others = alldata.others;
                    $scope.formdata.type = alldata.type;
                }
                else
                {
                    $location.path('/job/3/'+$scope.formdata.requisitionId);
                } 
            }, function myError(response) {
                console.log(response);
            });
            //get jobdata end

            //get qualification data start
            $http({
                method : "POST",
                url : urlService.apiurl+'RequisitionQualificationDetails/getData',
                data : {"requisitionId" :$routeParams.requisitionId},
            }).then(function mySuccess(response) {
                //console.log(response.data.data);
                var alldata = response.data.data;
                if(alldata)
                {
                    $scope.formdata.qualificationId = alldata.id;
                    $scope.formdata.educationalQualification = alldata.educationalQualification;
                    $scope.formdata.experience = alldata.experience;
                }
                else
                {
                    $location.path('/qualification/4/'+$scope.formdata.requisitionId);
                } 
                
            }, function myError(response) {
                console.log(response);
            });
        }

        $scope.checkAccess = function(level){
            if(level.length > 0)
            {
                var checkFlag = 0; 
                for(var i = 0;i < level.length; i++)
                {
                    if(level[i].empCode == empCode)
                    {
                        checkFlag = 1;
                        if(level[i].status == 3 || level[i].status == 4)
                        {
                            //just show the remarks in readonly mode and disable button
                            $scope.approvalRemarks.push({"srNo" : i+1,"remarks" : level[i].remarks});
                            $scope.remarksDiv = false;
                        }
                        else
                        {
                            //textbox will appear and button will be active
                            $scope.remarksDiv = true;
                        }
                        break;
                    }
                    else
                    {
                        if(level[i].status == 3)
                        {
                            $scope.approvalRemarks.push({"srNo" : i+1,"remarks" : level[i].remarks});
                            //just show the remarks in readonly mode and disable button
                        }
                        else
                        {
                            $scope.mainContainer = false;
                            $scope.message = true;
                        }
                    }
                }

                if(checkFlag == 0)
                {
                    $scope.mainContainer = false;
                    $scope.message = true;
                }
            }
        }

        $scope.updateLevelStatus = function(status){
            $scope.error = {};
            if($scope.formdata.remarks == '')
            {
                $scope.error.remarks_err = errorStrings.required;
            }
            else
            {
                if(typeof $routeParams.requisitionId !== 'undefined')
                {
                    var approvalData =  {
                                            "requisitionId" : $routeParams.requisitionId,
                                            "status" : status,
                                            "remarks" : $scope.formdata.remarks,
                                            "id" : $scope.formdata.detailsId,
                                            "empCode" : empCode,
                                            "createdBy" : $scope.createdBy,
                                            "leveldata" : $scope.levels
                                        }
                    rfrApiServices.RequisitionDetails_updateApprovalStatus(approvalData, function(error,data){
                        if(error) {
                            alert("Something went wrong");
                        } else {


                            alert("Your action has been submitted successfully");
                            $location.path('/approve/6/'+$routeParams.requisitionId);
                        }
                    });
                }
                else
                {
                    $location.path('/');
                }
            }            
        }
        ////////////////////////////////////// RFR (approval) ends /////////////////////////////////////////////
    });

})( angular, app );