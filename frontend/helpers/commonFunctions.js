app.service('commonFunctions', function() {
    this.getDate = function (x) {
        var dateObj = new Date(x);
        var month = dateObj.getUTCMonth() + 1; //months from 1-12
        var day = dateObj.getUTCDate();
        var year = dateObj.getUTCFullYear();

        if(parseInt(month) < 10)
        {
            month = "0"+month;
        }

        if(parseInt(day) < 10)
        {
            day = "0"+day;
        }

        return newdate = year + "-" + month + "-" + day;
        //return newdate = month + "/" + day + "/" + year;
    }

    this.getDateTime = function (x) {
        var dateObj = new Date(x);
        return date.toLocaleString();
    }
});