'use strict';

module.exports = function(RequisitionQualificationDetails) {

	RequisitionQualificationDetails.saveData = function(data, cb) {
		console.log(data);
        var username = "test";
        const reqruisitionQualificationData = {
            "requisitionId" : data.requisitionId,
            "educationalQualification": data.educationalQualification,
            "experience": data.experience,
            "status": data.status,
            "createdBy" : username,
            "createdOn" : new Date()
        }
        if(data.id != "0")
        {
            reqruisitionQualificationData.id = data.id;
            reqruisitionQualificationData.updatedBy = username;
            reqruisitionQualificationData.updatedOn = new Date();
        }
        RequisitionQualificationDetails.upsert(reqruisitionQualificationData,function(err,callbackData) {
            if (err) {
                cb({"success": "false","message": "checklog"},err);
            }

            cb("",callbackData );
        })
        
    }

    RequisitionQualificationDetails.getData = function(requisitionId, cb) {
        console.log(requisitionId);
        RequisitionQualificationDetails.findOne({"where":{"requisitionId" : requisitionId}},function(err,result) {
            if (err) {
                cb({"success": "false","message": "checklog"},err);
            }

            cb("",result );
        })   
    }

    RequisitionQualificationDetails.remoteMethod(
        'getData',
        {
            accepts: {arg: 'requisitionId', type: 'string'},
            returns: {arg: 'data', type: 'object'}
        }
    );

	RequisitionQualificationDetails.remoteMethod(
        'saveData',
        {
            accepts: {arg: 'data', type: 'object'},
            returns: {arg: 'data', type: 'object'}
        }
    );

};
