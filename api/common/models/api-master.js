'use strict';

var  executeQuery = function( query,callback){
    var takeData = []
    var sql = require('mssql');
    var config = {
        server: '35.154.73.53',
        database: 'EMPKONNECT_CONFIGDEV',
        user: 'dev_honohr',
        password: 'E4G6<gpd3Euj=k8U',
        port :21443,

        options: {
            encrypt: true // Use this if you're on Windows Azure
        }
    }
    var pool1 = new sql.ConnectionPool(config, function(err){

        pool1.request() // or: new sql.Request(pool1)
            .query(query, function(err, result) {

                if (err) {
                    console.log("Error while querying database :- " + err);
                    pool1.close()
                    return callback(err, null);
                }
                return callback(null, result);
            })

    })

}



var takeAction = function(takeFullDatabaseData,dataParams,callback){
    var sql = require('mssql');
    var takeAllOu = {
        "function":"functMAST",
        "grade": "grdmast",
        "buss": "bussmast",
        "location":"locmast",
        "band":"bandmast",
        "division":"divimast",
        "cost":"costmast",

    }
    var takeDatabaseDetails = takeFullDatabaseData.recordset[0]
    var config1 = {
        server: '35.154.73.53',
        database: takeDatabaseDetails.db_name,
        user: takeDatabaseDetails.dB_user,
        password: takeDatabaseDetails.db_password,
        port :21443
    };


        var Pool2 = new sql.ConnectionPool(config1, function(err){
            var queryCountry = "select * from "+takeAllOu[dataParams.action]
            Pool2.request() // or: new sql.Request(pool1)

                .query(queryCountry, function(err, result) {

                    if (err) {
                        console.log("Error while connecting database :- " + err);
                        Pool2.close()
                        return callback(err, null);
                    }
                    else {
                        // query to the database
                        Pool2.close()
                        return callback(null, result.recordsets);

                    }
                })

        })



}
var takeActionForLocation = function(takeFullDatabaseData,dataParams,callback){
    var sql = require('mssql');

    var takeDatabaseDetails = takeFullDatabaseData.recordset[0]
    var config1 = {
        server: '35.154.73.53',
        database: takeDatabaseDetails.db_name,
        user: takeDatabaseDetails.dB_user,
        password: takeDatabaseDetails.db_password,
        port :21443
    };
    if(dataParams.action ==='country'){

        var countryData = new sql.ConnectionPool(config1, function(err){
            var queryCountry = "select CountryID,Country_CODE,Country_NAME from countryMast where Status=1"
            countryData.request() // or: new sql.Request(pool1)

                .query(queryCountry, function(err, result) {

                    if (err) {
                        console.log("Error while connecting database :- " + err);
                        countryData.close()
                        return callback(err, null);

                    }
                    else {
                        // query to the database

                        countryData.close()
                        return callback(null, result.recordsets[0]);

                    }
                })

        })
    }
    if(dataParams.action ==='state'){

        var stateData = new sql.ConnectionPool(config1, function(err){

            var queryState = "select * from statemast where Country_Id='"+dataParams.countryId+"'"
            stateData.request() // or: new sql.Request(pool1)

                .query(queryState, function(err, result) {

                    if (err) {
                        console.log("Error while connecting database :- " + err);
                        stateData.close()
                        return callback(err, null);

                    }
                    else {
                        // query to the database

                        stateData.close()
                        return callback(null, result.recordsets[0]);
                    }
                })

        })
    }
    if(dataParams.action ==='city'){

        var cityData = new sql.ConnectionPool(config1, function(err){
            var queryCity =  "select CityID,City_NAME from citymast where Status=1 AND Country_ID='"+dataParams.countryId+"' AND State_Id='"+dataParams.stateId+"'"
            cityData.request() // or: new sql.Request(pool1)

                .query(queryCity, function(err, result) {

                    if (err) {
                        console.log("Error while connecting database :- " + err);
                        cityData.close()
                        return callback(err, null);

                    }
                    else {
                        // query to the database

                        cityData.close()
                        return callback(null, result.recordsets[0]);
                    }
                })

        })
    }



}
module.exports = function(Apimaster) {
    Apimaster.getAccessToken = function(id, cb) {


        Apimaster.findOne({
            "where": {
                "compCode":id
            }
        }, function(err, user) {
            if (err) {
                cb("",err);
            }
            if(user && user.length != 0 ) {

                user.createAccessToken(5000, function(err, token){
                    cb("",token);
                });
            }
            else
                cb("",{"status":false,"msg":"this company not exist"})

        })
    }
    Apimaster.getCompData = function(id, cb) {



        var sql = require('mssql');





        Apimaster.findOne({
            "where": {
                "id":id.id
            }
        }, function(err, user) {
            if (err) {
                //cb("",err);
            }
            else{
                if(user && user.length != 0 ) {
                    //console.log('sdfdsfsdfdsfdsf')
                    var mainQuery = "select * from compmast where  comp_code='"+user.compCode+"'"

                    executeQuery(mainQuery, function (err, rows) {
                        if (!err) {

                            if(id.action === 'country' || id.action === 'city' || id.action === 'state' ){
                                takeActionForLocation(rows,id, function (error,callback2) {

                                    if(error)
                                        cb("", error)
                                    else
                                        cb("",callback2)
                                })
                            }
                            else{
                                takeAction(rows,id, function (error,callback2) {

                                    if(error)
                                        cb("", error)
                                    else
                                        cb("",callback2)
                                })
                            }


                        }
                        else {
                            console.log(err)
                        }
                    });


                }
                //else
                    //cb("","no data found")
            }


        })

    }


    Apimaster.remoteMethod(
        'getAccessToken',
        {
            accepts: {arg: 'id', type: 'string'},
            returns: {arg: 'data', type: 'object'}
        }
    );
    Apimaster.remoteMethod(
        'updateStatus',
        {
            accepts: {arg: 'status', type: 'boolean'},
            returns: {arg: 'data', type: 'object'}
        }
    );

    Apimaster.remoteMethod(
        'getCompData',
        {
            accepts: {arg: 'takeData', type: 'object'},
            returns: {arg: 'data', type: 'object'},
            http:{
                path:'/getCompData',verb:'get'
            }
        }
    );

};
