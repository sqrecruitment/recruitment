'use strict';

module.exports = function(Review) {
  Review.beforeRemote('create', function(context, user, next) {
    context.args.data.date = Date.now();
    context.args.data.publisherId = context.req.accessToken.userId;
    next();
  });

    Review.greet = function(msg, cb) {
    cb(null, 'Greetings... ' - msg);
  };

	  Review.remoteMethod(
	    'greet', {
	      accepts: {
	        arg: 'msg',
	        type: 'string'
	      },
	      returns: {
	        arg: 'greeting',
	        type: 'string'
	      }
	    }
	  );
};
