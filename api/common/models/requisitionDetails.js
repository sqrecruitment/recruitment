'use strict';

const nodemailer = require('nodemailer');

module.exports = function(RequisitionDetails) {

	RequisitionDetails.saveData = function(data, cb) {
		//console.log(data);
        var username = "test";
        const reqruisitionDetailsData = {
                                            "requisitionId" : data.requisitionId,
                                            "requestNo": data.requestNo,
                                            "requestDate": data.requestDate,
                                            "requesterName": data.requesterName,
                                            "company": data.company,
                                            "businessUnit": data.businessUnit,
                                            "grade": data.grade,
                                            "gradeLevel": data.gradeLevel,
                                            "openings": data.openings,
                                            "hiringType": data.hiringType,
                                            "replacementFor": data.replacementFor,
                                            "criticality": data.criticality,
                                            "closingDate": data.closingDate,
                                            "startingDate": data.startingDate,
                                            "targetHireDate": data.targetHireDate,
                                            "targetEndDate": data.targetEndDate,
                                            "isBudgeted": data.isBudgeted,
                                            "maxBudget": data.maxBudget,
                                            "minBudget": data.minBudget,
                                            "levels": data.levels,
                                            "stages": data.stages,
                                            "status": data.status,
                                            "createdBy" : username,
                                            "createdOn" : new Date()
                                        }
        if(data.id != "0")
        {
            reqruisitionDetailsData.id = data.id;

            reqruisitionDetailsData.updatedBy = username;
            reqruisitionDetailsData.updatedOn = new Date();

        }
        
        RequisitionDetails.upsert(reqruisitionDetailsData,function(err,callbackData) {
            if (err) {
                cb({"success": "false","message": "checklog"},err);
            }

            cb("",callbackData );
        })
    }

    RequisitionDetails.getData = function(requisitionId, cb) {
        console.log(requisitionId);
        RequisitionDetails.findOne({"where":{"requisitionId" : requisitionId}},function(err,result) {
            if (err) {
                cb({"success": "false","message": "checklog"},err);
            }

            cb("",result );
        })   
    }

    RequisitionDetails.updateApprovalStatus = function(data, cb) {
        
        var username = "test";
        var levelsdata = data.leveldata;
        var isUpdateStatus = 1;
        var finalStatus = 0;
        var newleveldata = [];
        var nextMailto = '';
        for(var i= 0;i < levelsdata.length; i++)
        {
            if(levelsdata[i].empCode == data.empCode)
            {
                levelsdata[i].status = data.status;
                levelsdata[i].updatedBy = username;
                levelsdata[i].remarks = data.remarks;
                levelsdata[i].updatedOn = new Date();
            }

            newleveldata.push(levelsdata[i]);

            if(newleveldata[i].status == 0 || newleveldata[i].status == "0" || newleveldata[i].status == 4)
            {
                if(nextMailto == ''){
                    nextMailto = newleveldata[i].empCode;
                }
                isUpdateStatus = 0;
            }
        }

        if(isUpdateStatus == 1 && data.status == 3)
        {
            finalStatus = 3;
        }
        console.log(finalStatus);

        const requisitionDetailsData = {
            "levels" : newleveldata,
            "id" : data.id,
            "requisitionId" : data.requisitionId
        }

        console.log(requisitionDetailsData);

        RequisitionDetails.upsert(requisitionDetailsData,function(err,callbackData) {
            if (err) {
                cb({"success": "false","message": "checklog"},err);
            }

            if(data.status == 3) //Approved
            {
                //here you have to send email to both initiater and approver
                var sendTo = [data.createdBy];
                if(finalStatus == 3)
                {
                    sendTo.push = nextMailto;
                }

                for(var j = 0; j < sendTo.length; j++)
                {
                    var empCode = sendTo[i];
                    if(typeof empCode !== 'undefined')
                    {
                        var empName = "Manoj Gupta";
                        var empEmail = "manoj.gupta@sequelone.com";

                        var username = "HR";

                        var emailTo = empEmail;
                        var link = data.hosturl+"approve/6/"+data.requisitionId;
                        var subject = "NoReply Dated @Date";
                        var message = "Dear "+empName+"("+empCode+"),<br><br>";
                        message+="This mail is to inform you that new RFR [@RFR ID] has been approved on @Date <br>";
                        message+="Please find the below link for your action<br>";
                        message+="Link URL : <a href='"+link+"'>Click Here</a>";
                        message+="<br><br><br>Thanks<br>";
                        message+=username;

                        RequisitionDetails.sendEmail(emailTo,subject,message);
                    }
                }

                
            }
            if(data.status == 4) //rejected
            {           
                //here you have to send email to initiater     
                var empCode = data.createdBy;
                var empName = "Manoj Gupta";
                var empEmail = "manoj.gupta@sequelone.com";

                var username = "initiater";

                var emailTo = empEmail;
                var link = data.hosturl+"summary/5/"+data.requisitionId;
                var subject = "NoReply Dated @Date";
                var message = "Dear "+empName+"("+empCode+"),<br><br>";
                message+="This mail is to inform you that new RFR [@RFR ID] has been rejected on @Date <br>";
                message+="Please find the below link for your action<br>";
                message+="Link URL : <a href='"+link+"'>Click Here</a>";
                message+="<br><br><br>Thanks<br>";
                message+=username;

                RequisitionDetails.sendEmail(emailTo,subject,message);
            }
           
            cb("",{"finalStatus" : finalStatus});
        })
    }

    RequisitionDetails.sendEmail = function(emailTo,subject,message, cb) {
    
        var transporter = nodemailer.createTransport({
                                                        service: 'gmail',
                                                        auth:   {
                                                                    user: 'manoj.gupta@sequelone.com',
                                                                    pass: 'manoj@sq123'
                                                                }
                                                    });

        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

        // setup email data with unicode symbols
        let mailOptions = {
            from: '"SequelOne" <donotreply@sequelone.com>', // sender address
            to: emailTo, // list of receivers
            subject: subject, // Subject line
            text: message, // plain text body
            html: message // html body
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions,  function(error, info) {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        })


            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

    }

    RequisitionDetails.remoteMethod(
        'updateApprovalStatus',
        {
            accepts: {arg: 'data', type: 'object'},
            returns: {arg: 'data', type: 'object'}
        }
    );

    RequisitionDetails.remoteMethod(
        'sendEmail',
        {
            accepts: [{arg: 'to', type: 'string'},{arg: 'body', type: 'string'},{arg: 'subject', type: 'string'}],
            returns: {arg: 'data', type: 'object'}
        }
    );

    RequisitionDetails.remoteMethod(
        'getData',
        {
            accepts: {arg: 'requisitionId', type: 'string'},
            returns: {arg: 'data', type: 'object'}
        }
    );

	RequisitionDetails.remoteMethod(
        'saveData',
        {
            accepts: {arg: 'data', type: 'object'},
            returns: {arg: 'data', type: 'object'}
        }
    );

};
