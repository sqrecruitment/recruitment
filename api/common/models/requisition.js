'use strict';

const nodemailer = require('nodemailer');

module.exports = function(Requisition) {

    Requisition.saveData = function(data, cb) {
        var username = "test";
        //console.log(data);
        const requisitionData = {
            "title": data.title,
            "positionType": data.positionType,
            "status" : data.status,
            "other" : data.other,
            "createdBy" : username,
            "createdOn" : new Date()
        }

        if(data.id != "0")
        {
            requisitionData.id = data.id;
            requisitionData.updatedBy = username;
            requisitionData.updatedOn = new Date();
        }
        console.log(requisitionData);
        Requisition.upsert(requisitionData,function(err,callbackData) {
            if (err) {
                cb({"success": "false","message": "checklog"},err);
            }

            cb("",callbackData );
        })
        
    }

    Requisition.getData = function(requisitionId, cb) {
        //console.log(requisitionId);
        //var query = {"_id" : requisitionId};
        /*var emailTo = 'manoj.gupta@sequelone.com';
        var subject = "testing subject";
        var message = "hello mail function";
        Requisition.sendEmail(emailTo,subject,message);*/
        
        Requisition.findOne({"where":{"id" : requisitionId}},function(err,result) {
            if (err) {
                cb({"success": "false","message": "checklog"},err);
            }

            cb("",result );
        })   
    }

    Requisition.updateStatus = function(data, cb) {
        console.log(data);
        //var query = {"_id" : requisitionId};
        var username = "test";
        const requisitionData = {
            "status" : data.status,
            "id" : data.requisitionId,
            "updatedBy" : username,
            "updatedOn" : new Date()
        }
        Requisition.upsert(requisitionData,function(err,callbackData) {
            if (err) {
                cb({"success": "false","message": "checklog"},err);
            }

            //trigger email here for approval and stages.
            console.log(data.levels);
            if(data.status  == 2)
            {
                var empCode = data.levels[0].empCode;
                console.log(empCode);
                if(typeof empCode === 'undefined')
                {
                    cb({"success": "false","message": "No approval level found"},err);
                }
                
                var empName = "Manoj Gupta";
                var empEmail = "manoj.gupta@sequelone.com";

                var username = "HR";

            
                var emailTo = empEmail;
                var link = data.hosturl+"approve/6/"+data.requisitionId;
                var subject = "NoReply Dated @Date";
                var message = "Dear "+empName+"("+empCode+"),<br><br>";
                message+="This mail is to inform you that new RFR [@RFR ID] has been created on @Date <br>";
                message+="Please find the below link for your action<br>";
                message+="Link URL : <a href='"+link+"'>Click Here</a>";
                message+="<br><br><br>Thanks<br>";
                message+=username;

                Requisition.sendEmail(emailTo,subject,message);
            }
            cb("",callbackData );
        })
    }

    Requisition.sendEmail = function(emailTo,subject,message, cb) {
    
        var transporter = nodemailer.createTransport({
                                                        service: 'gmail',
                                                        auth:   {
                                                                    user: 'manoj.gupta@sequelone.com',
                                                                    pass: 'manoj@sq123'
                                                                }
                                                    });

        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

        // setup email data with unicode symbols
        let mailOptions = {
            from: '"SequelOne" <donotreply@sequelone.com>', // sender address
            to: emailTo, // list of receivers
            subject: subject, // Subject line
            text: message, // plain text body
            html: message // html body
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

            // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
            // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
        });
    }

    Requisition.updateApprovalStatus = function(data, cb) {
        
        var username = "test";
        var levelsdata = data.leveldata;
        var isUpdateStatus = 1;
        for(var i= 0;i < levelsdata.length; i++)
        {
            if(levelsdata[i].empCode == data.empCode)
            {
                levelsdata[i].status = data.status;
                levelsdata[i].updatedBy = username;
                levelsdata[i].updatedOn = new Date();
            }

            if(levelsdata[i].status == 0 || levelsdata[i].status == 4)
            {
                isUpdateStatus = 0;
            }
        }

        console.log(levelsdata);

        const requisitionData = {
            "levels" : levelsdata,
            "id" : data.requisitionId,
            "updatedBy" : username,
            "updatedOn" : new Date()
        }

        if(isUpdateStatus == 1 && data.status == 3)
        {
            requisitionData.status = 3;
        }
        console.log(requisitionData);

        /*Requisition.upsert(requisitionData,function(err,callbackData) {
            if (err) {
                cb({"success": "false","message": "checklog"},err);
            }

            if(data.status == 3) //Approved
            {
                var empCode = data.levels[1].empCode;
                console.log(empCode);
                if(typeof empCode === 'undefined')
                {
                    cb({"success": "false","message": "No approval level found"},err);
                }
                
                var empName = "Manoj Gupta";
                var empEmail = "manoj.gupta@sequelone.com";

                var username = "HR";

                var emailTo = empEmail;
                var link = data.hosturl+"approve/6/"+data.requisitionId;
                var subject = "NoReply Dated @Date";
                var message = "Dear "+empName+"("+empCode+"),<br><br>";
                message+="This mail is to inform you that new RFR [@RFR ID] has been approved on @Date <br>";
                message+="Please find the below link for your action<br>";
                message+="Link URL : <a href='"+link+"'>Click Here</a>";
                message+="<br><br><br>Thanks<br>";
                message+=username;

                Requisition.sendEmail(emailTo,subject,message);
            }
            if(data.status == 4) //rejected
            {           
                //here you have to send email to initiater     
                var empName = "Manoj Gupta";
                var empEmail = "manoj.gupta@sequelone.com";

                var username = "initiater";

                var emailTo = empEmail;
                var link = data.hosturl+"summary/5/"+data.requisitionId;
                var subject = "NoReply Dated @Date";
                var message = "Dear "+empName+"("+empCode+"),<br><br>";
                message+="This mail is to inform you that new RFR [@RFR ID] has been rejected on @Date <br>";
                message+="Please find the below link for your action<br>";
                message+="Link URL : <a href='"+link+"'>Click Here</a>";
                message+="<br><br><br>Thanks<br>";
                message+=username;

                Requisition.sendEmail(emailTo,subject,message);
            }
           
            cb("",callbackData );
        })*/
    }

    Requisition.remoteMethod(
        'updateApprovalStatus',
        {
            accepts: {arg: 'data', type: 'object'},
            returns: {arg: 'data', type: 'object'}
        }
    );

    Requisition.remoteMethod(
        'sendEmail',
        {
            accepts: [{arg: 'to', type: 'string'},{arg: 'body', type: 'string'},{arg: 'subject', type: 'string'}],
            returns: {arg: 'data', type: 'object'}
        }
    );

    Requisition.remoteMethod(
        'updateStatus',
        {
            accepts: {arg: 'data', type: 'object'},
            returns: {arg: 'data', type: 'object'}
        }
    );

    Requisition.remoteMethod(
        'saveData',
        {
            accepts: {arg: 'data', type: 'object'},
            returns: {arg: 'data', type: 'object'}
        }
    );

    Requisition.remoteMethod(
        'getData',
        {
            accepts: {arg: 'requisitionId', type: 'string'},
            returns: {arg: 'data', type: 'object'}
        }
    );

};
