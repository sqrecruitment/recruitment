'use strict';

module.exports = function(Vendorsmaster) {

    Vendorsmaster.getAllVendors = function (authData,cb) {
        Vendorsmaster.find({},function(err,data){
            if(data && data.length != 0 ) {

                cb("",data)
            }
            else
                cb("","no data found")
        })
    }

    Vendorsmaster.getVendorAccessToken = function (id,cb) {
        Vendorsmaster.findOne({
            "where": {
                "id":id
            }
        },function(err, vendor) {
            if (err) {
                cb("",err);
            }
            if(vendor && vendor.length != 0 ) {

                vendor.createAccessToken(5000, function(err, token){
                    cb("",token);
                });
            }
            else
                cb("",{"status":false,"msg":"this Vendor not exist"})

        })
    }

    Vendorsmaster.remoteMethod(
        'getVendorAccessToken',
        {
            accepts: {arg: 'id', type: 'string'},
            returns: {arg: 'data', type: 'object'}
        }
    );
    Vendorsmaster.remoteMethod(
        'getAllVendors',
        {
            accepts: {arg: 'auth_data', type: 'object'},
            returns: {arg: 'data', type: 'object'}
        }
    );


};
