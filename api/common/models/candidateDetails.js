/**
 * Created by pramod on 12-Jun-18.
 */

'use strict';

module.exports = function(CandidateDetails) {
    CandidateDetails.checkValidation = function(data, cb) {
        console.log(data);
    }

    CandidateDetails.remoteMethod('checkValidation', {
        accepts:{'arg':'data','type':'object'},
        returns:{'arg':'result','type':'object'}
    });
};
