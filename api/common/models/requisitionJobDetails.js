'use strict';

module.exports = function(RequisitionJobDetails) {

	RequisitionJobDetails.saveData = function(data, cb) {
		console.log(data);
        var username = "test";
        const reqruisitionJobData = {
            "requisitionId" : data.requisitionId,
            "position": data.position,
            "type": data.type,
            "location": data.location,
            "description": data.description,
            "skills": data.skills,
            "status" : data.status,
            "others" : data.others,
            "createdBy" : username,
            "createdOn" : new Date()
        }

        if(data.id != "0")
        {
            reqruisitionJobData.id = data.id;
            reqruisitionJobData.updatedBy = username;
            reqruisitionJobData.updatedOn = new Date();
        }
        RequisitionJobDetails.upsert(reqruisitionJobData,function(err,callbackData) {
            if (err) {
                cb({"success": "false","message": "checklog"},err);
            }

            cb("",callbackData );
        })
        
    }

    RequisitionJobDetails.getData = function(requisitionId, cb) {
        console.log(requisitionId);
        RequisitionJobDetails.findOne({"where":{"requisitionId" : requisitionId}},function(err,result) {
            if (err) {
                cb({"success": "false","message": "checklog"},err);
            }

            cb("",result );
        })   
    }

    RequisitionJobDetails.remoteMethod(
        'getData',
        {
            accepts: {arg: 'requisitionId', type: 'string'},
            returns: {arg: 'data', type: 'object'}
        }
    );

	RequisitionJobDetails.remoteMethod(
        'saveData',
        {
            accepts: {arg: 'data', type: 'object'},
            returns: {arg: 'data', type: 'object'}
        }
    );

};
