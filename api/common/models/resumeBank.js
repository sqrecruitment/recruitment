/**
 * Created by pramod on 12-Jun-18.
 */
'use strict';

module.exports = function(resumeBank) {
    resumeBank.createContainerNew = function(options,cb){
        resumeBank.createContainer(options, function(err,container){
            cb(err,container);
        });
    }
    resumeBank.remoteMethod("createContainerNew",{
        accepts:{arg:"data",type:"object"},
        returns:{arg:"data",type:"object"}
    })
};
