module.exports = function(app) {
  // Install a `/` route that returns server status
  app.dataSources.uploads.connector.getFilename = function(file,req,res){
    var extnsionName = file.name.split('.').pop();
    var ext = '.'+extnsionName;
    var fileName = file.name.split(ext)[0];
    var timeUX = new Date().getTime();
      var time = new Date();
      var fullDate = time.getFullYear()+"_"+time.getMonth()+"_"+time.getDate()+"_"+time.getHours()+"_"+time.getMinutes()+"_"+time.getSeconds();
      return fullDate+"_"+fileName+"_" + timeUX + '_.' + extnsionName;
  }
};